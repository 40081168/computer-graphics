#include <graphics_framework.h>
#include <glm\glm.hpp>

using namespace std;
using namespace graphics_framework;
using namespace glm;

map<string, mesh> meshes;
effect eff;
texture tex;
mesh m;
//target_camera cam;
chase_camera cam;
double cursor_x = 0.0;
double cursor_y = 0.0;

// Main textures
//array<texture, 2> texs;

// Blend map
//texture blend_map;

bool initialise()
{
	// ******************************
	// Capture initial mouse position
	// ******************************
	glfwGetCursorPos(renderer::get_window(), &cursor_x, &cursor_y);

	return true;
}
bool load_content()
{
	// Create plane mesh
	//meshes["plane"] = mesh(geometry_builder::create_plane());

	meshes["plane"] = mesh(geometry("..\\resources\\models\\Terrain1.obj"));

	// Construct geometry object
	geometry geom;
	geom.set_type(GL_QUADS);
	vector<vec3> positions
	{
		vec3(1.0f, 1.0f, 0.0f),
		vec3(-1.0f, 1.0f, 0.0f),
		vec3(-1.0f, -1.0f, 0.0f),
		vec3(1.0f, -1.0f, 0.0f)
	};
	vector<vec2> tex_coords
	{
		vec2(2.0f, 2.0f),
		vec2(-1.0f, 2.0f),
		vec2(-1.0f, -1.0f),
		vec2(2.0f, -1.0f)
	};
	// Add to the geometry
	geom.add_buffer(positions, BUFFER_INDEXES::POSITION_BUFFER);

	// *****************************************
	// Add texture coordinate buffer to geometry
	// *****************************************
	geom.add_buffer(tex_coords, BUFFER_INDEXES::TEXTURE_COORDS_0);

	// Create mesh to chase
	meshes["chaser"] = mesh(geometry_builder::create_box());
	meshes["chaser"].get_transform().position = vec3(-20.0f, 75.0f, 0.0f);

	// *************
	// Load in model
	// *************
	//m = mesh(geometry("..\\resources\\models\\obj_test.obj"));
	//m = mesh(geometry("..\\resources\\models\\Terrain1.obj"));
	//m = mesh(geometry("..\\resources\\models\\Ocean.obj"));

	// ***************
	// Load in texture
	// ***************
	tex = texture("..\\resources\\textures\\DESERTTEST.jpg");
	
	// Load in shaders
	eff.add_shader("..\\resources\\shaders\\simple_texture.vert", GL_VERTEX_SHADER);
	eff.add_shader("..\\resources\\shaders\\simple_texture.frag", GL_FRAGMENT_SHADER);
	
	// Build effect
	eff.build();
	
	// Set camera properties
	//cam.set_position(vec3(200.0f, 200.0f, 200.0f));
	//cam.set_target(vec3(0.0f, 0.0f, 0.0f));
	
	// Set camera properties
	cam.set_pos_offset(vec3(0.0f, 4.0f, 20.0f));
	cam.set_springiness(0.5f);
	cam.move(meshes["chaser"].get_transform().position, eulerAngles(meshes["chaser"].get_transform().orientation));
	
	auto aspect = static_cast<float>(renderer::get_screen_width()) / static_cast<float>(renderer::get_screen_height());
	cam.set_projection(quarter_pi<float>(), aspect, 2.414f, 2000.0f);
	return true;
}
//bool update1(float delta_time)
//{
//	// Update the camera
//	cam.update(delta_time);
//	return true;
//}
bool render()
{
	for (auto &e : meshes)
	{
		auto m = e.second;
		// Bind effect
		renderer::bind(eff);
		
		// Create MVP matrix
		auto M = m.get_transform().get_transform_matrix();
		auto V = cam.get_view();
		auto P = cam.get_projection();
		auto MVP = P * V * M;
		
		// Set MVP matrix uniform
		glUniformMatrix4fv(
			eff.get_uniform_location("MVP"),
			1,
			GL_FALSE,
			value_ptr(MVP));

		// Bind and set texture
		renderer::bind(tex, 0);
		glUniform1i(eff.get_uniform_location("tex"), 0);

		// Render mesh
		renderer::render(m);
	}
	return true;
}
bool update(float delta_time)
{
	// The target object
	static mesh &target_mesh = meshes["chaser"];

	// The ratio of pixels to rotation - remember the fov
		static double ratio_width = quarter_pi<float>() / static_cast<float>(renderer::get_screen_width());
		static double ratio_height = (quarter_pi<float>() * (static_cast<float>(renderer::get_screen_height()) / static_cast<float>(renderer::get_screen_width()))) / static_cast<float>(renderer::get_screen_height());

	double current_x;
	double current_y;
	
	// *******************************
	// Get the current cursor position
	// *******************************
	glfwGetCursorPos(renderer::get_window(), &current_x, &current_y);

	// ***************************************************
	// Calculate delta of cursor positions from last frame
	// ***************************************************
	double delta_x = current_x - cursor_x;
	double delta_y = current_y - cursor_y;

	// ****************************************************************************
	// Multiply deltas by ratios and delta_time - gets actual change in orientation
	// ****************************************************************************
	delta_x *= ratio_width;
	delta_y *= ratio_height;

	// *************************
	// Rotate cameras by delta
	// x - delta_y
	// y - delta_x
	// z - 0
	// *************************
	cam.rotate(vec3(delta_y, delta_x, 0.0f));

	// ************************************
	// Use keyboard to move the target_mesh
	// - WSAD
	// ************************************
	if (glfwGetKey(renderer::get_window(), 'W'))
		target_mesh.get_transform().translate(target_mesh.get_transform().orientation * vec3(0.0f, 0.0f, -1.0f) * 15.0f * delta_time);
	if (glfwGetKey(renderer::get_window(), 'S'))
		target_mesh.get_transform().translate(target_mesh.get_transform().orientation * vec3(0.0f, 0.0f, 1.0f) * 5.0f * delta_time);
	if (glfwGetKey(renderer::get_window(), 'A'))
		target_mesh.get_transform().translate(target_mesh.get_transform().orientation * vec3(-1.0f, 0.0f, 0.0f) * 1.0f * delta_time);
	if (glfwGetKey(renderer::get_window(), 'D'))
		target_mesh.get_transform().translate(target_mesh.get_transform().orientation * vec3(1.0f, 0.0f, 0.0f) * 1.0f * delta_time);

	// *************************************************
	// Move camera - update target position and rotation
	// *************************************************
	cam.move(target_mesh.get_transform().position, eulerAngles(target_mesh.get_transform().orientation));

	// ***********************************
	// Use keys to update transform values
	// WSAD - movement
	// Cursor - rotation
	// O decrease scale, P increase scale
	// ***********************************
	/*if (glfwGetKey(renderer::get_window(), 'W'))
		m.get_transform().position -= vec3(0.0f, 0.0f, 5.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), 'S'))
		m.get_transform().position += vec3(0.0f, 0.0f, 5.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), 'A'))
		m.get_transform().position -= vec3(5.0f, 0.0f, 0.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), 'D'))
		m.get_transform().position += vec3(5.0f, 0.0f, 0.0f) * delta_time;
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_UP))
		m.get_transform().rotate(vec3(-pi<float>() * delta_time, 0.0f, 0.0f));
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_DOWN))
		m.get_transform().rotate(vec3(pi<float>() * delta_time, 0.0f, 0.0f));
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_RIGHT))
		m.get_transform().rotate(vec3(0.0f, 0.0f, -pi<float>() * delta_time));
	if (glfwGetKey(renderer::get_window(), GLFW_KEY_LEFT))
		m.get_transform().rotate(vec3(0.0f, 0.0f, pi<float>() * delta_time));
	if (glfwGetKey(renderer::get_window(), 'O'))
		m.get_transform().scale -= 1.0f * delta_time;
	if (glfwGetKey(renderer::get_window(), 'P'))
		m.get_transform().scale += 1.0f * delta_time;*/

	//**********************************
	// - QE rotate on y-axis
	// **********************************
	if (glfwGetKey(renderer::get_window(), 'Q'))
		target_mesh.get_transform().rotate(vec3(0.0f, half_pi<float>(), 0.0f) * delta_time);
	if (glfwGetKey(renderer::get_window(), 'E'))
		target_mesh.get_transform().rotate(vec3(0.0f, -half_pi<float>(), 0.0f) * delta_time);

	// Update the camera
	cam.update(delta_time);

	// *****************
	// Update cursor pos
	// *****************
	cursor_x = current_x;
	cursor_y = current_y;


	return true;
}
void glClearColor()
{
	GLclampf	 red;
	GLclampf	 green;
	GLclampf	 blue;
	GLclampf	 alpha;
}
void main()
{
	// Create application
	app application;
	
	// Set load content, update and render methods
	application.set_load_content(load_content);
	application.set_initialise(initialise);
	application.set_update(update);
	application.set_render(render);
	
	// Run application
	application.run();
}